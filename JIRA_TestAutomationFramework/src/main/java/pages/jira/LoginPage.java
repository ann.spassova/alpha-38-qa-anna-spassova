package pages.jira;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseJiraPage {

    public LoginPage(WebDriver driver) {
                super(driver, "jira.homePage");
    }

    public void loginUser(String userKey) {
        String username = getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");

        actions.waitForElementVisible("jira.loginPage.loginButton");
        actions.clickElement("jira.loginPage.loginButton");

        actions.waitForElementClickable("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.loginSubmitButton");

        actions.waitForElementVisible("jira.projectsPage.avatar");
    }

}
