package test.cases.jira;

import org.junit.Test;
import pages.jira.ProjectPage;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class CreateStoryTest extends BaseTest {

    @Test
    public void createStoryTest() {
//        login();
        ProjectPage page = new ProjectPage(actions.getDriver());
        page.navigateToPage();
        page.createIssue("story");

        page.assertProjectSelected(getConfigPropertyByKey(("jira.projectName")));

//        actions.waitForElementVisible("jira.projectPage.viewIssueButton");
//        actions.clickElement("jira.projectPage.viewIssueButton");
//        issueId = actions.getDriver().findElement(
//                (By.xpath("//a[@target='_blank']"))).getText();
//        System.out.println(issueId);
    }
}
