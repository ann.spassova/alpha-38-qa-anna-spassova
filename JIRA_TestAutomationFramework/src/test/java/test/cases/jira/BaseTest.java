package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import pages.jira.LoginPage;
import pages.jira.ProjectPage;

public class BaseTest {

    static UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.homePage");
    }

    @Before
    public void login() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");
        loginPage.assertPageNavigated();
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }
}
