package test.cases.jira;

import org.junit.Test;
import pages.jira.ProjectPage;

import static test.cases.jira.BaseTest.actions;

public class LinkBugToStoryTest extends BaseTest{
    @Test
    public void linkBugToStoryTest() {
//        login();

        ProjectPage page = new ProjectPage(actions.getDriver());
        page.navigateToPage();
        page.linkStoryToBug();
    }
}
