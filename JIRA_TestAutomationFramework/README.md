**Instructions**
1. Clone repository
2. Open `JIRA_TestAutomationFramework` as a IntelliJ IDEA Project
3. Fill your credentials, project URL and project name in `src\test\resources\config.properties` (rows 5,6,8,9)
4. Run tests in `src\test\java\test.cases.jira` (one by one)